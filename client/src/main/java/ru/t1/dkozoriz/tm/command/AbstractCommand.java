package ru.t1.dkozoriz.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.api.model.ICommand;
import ru.t1.dkozoriz.tm.api.service.IEndpointLocator;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.ITokenService;

@Getter
@Setter
@Component
public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected final String name;

    @Nullable
    protected final String description;

    @Nullable
    protected final String argument;

    @Autowired
    protected ITokenService tokenService;

    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    protected AbstractCommand(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
        argument = null;
    }

    protected AbstractCommand(@NotNull String name, @Nullable String description, @Nullable String argument) {
        this.name = name;
        this.description = description;
        this.argument = argument;
    }

    public abstract void execute();

    @NotNull
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        final boolean hasName = !name.isEmpty();
        final boolean hasArgument = argument != null && !argument.isEmpty();
        final boolean hasDescription = description != null && !description.isEmpty();
        if (hasName) result += name;
        if (hasArgument) result += hasName ? ", " + argument : argument;
        if (hasDescription) result += ": " + description;
        return result;
    }

}
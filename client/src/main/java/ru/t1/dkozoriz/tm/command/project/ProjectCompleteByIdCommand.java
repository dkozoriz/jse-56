package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectCompleteByIdRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    public ProjectCompleteByIdCommand() {
        super("project-complete-by-id", "complete project by id.");
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        projectEndpoint.projectCompleteById(new ProjectCompleteByIdRequest(getToken(), id));
    }

}
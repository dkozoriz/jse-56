package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectCompleteByIndexRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;


@Component
public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    public ProjectCompleteByIndexCommand() {
        super("project-complete-by-index", "complete project by index.");
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        projectEndpoint.projectCompleteByIndex(new ProjectCompleteByIndexRequest(getToken(), index));
    }

}
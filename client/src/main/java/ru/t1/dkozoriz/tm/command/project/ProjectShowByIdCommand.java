package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectShowByIdRequest;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    public ProjectShowByIdCommand() {
        super("project-show-by-id", "show project by id.");
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final ProjectDto project =
                projectEndpoint.projectShowById(new ProjectShowByIdRequest(getToken(), id)).getProject();
        showProject(project);
    }

}
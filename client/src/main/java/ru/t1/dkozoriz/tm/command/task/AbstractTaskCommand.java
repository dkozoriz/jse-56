package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.api.endpoint.ITaskEndpoint;
import ru.t1.dkozoriz.tm.command.AbstractCommand;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDto;

import java.util.List;

@Component
public abstract class AbstractTaskCommand extends AbstractCommand {

    @Autowired
    protected ITaskEndpoint taskEndpoint;

    public AbstractTaskCommand(@NotNull String name, @Nullable String description) {
        super(name, description);
    }

    public Role[] getRoles() {
        return Role.values();
    }

    public String getArgument() {
        return null;
    }

    protected void showTask(@Nullable final TaskDto task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("STATUS: " + task.getStatus().getDisplayName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("PROJECTID: " + task.getProjectId());
    }

    protected void renderTasks(@Nullable final List<TaskDto> tasks) {
        if (tasks == null) return;
        int index = 1;
        for (final TaskDto task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

}
package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.task.TaskBindToProjectRequest;
import ru.t1.dkozoriz.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    public TaskBindToProjectCommand() {
        super("bind-task-to-project", "bind task to project.");
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        taskEndpoint.taskBindToProject(new TaskBindToProjectRequest(getToken(), projectId, taskId));
    }

}

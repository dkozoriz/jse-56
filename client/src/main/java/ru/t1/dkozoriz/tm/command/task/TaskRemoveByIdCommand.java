package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    public TaskRemoveByIdCommand() {
        super("task-remove-by-id", "remove task by id.");
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        taskEndpoint.taskRemoveById(new TaskRemoveByIdRequest(getToken(), id));
    }

}
package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.task.TaskShowAllByProjectIdRequest;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDto;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    public TaskShowByProjectIdCommand() {
        super("task-show-by-project-id", "show task by project id.");
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        @Nullable final List<TaskDto> tasks =
                taskEndpoint.taskShowAllByProjectId(new TaskShowAllByProjectIdRequest(getToken(),projectId)).getTaskList();
        renderTasks(tasks);
    }

}
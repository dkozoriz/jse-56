package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.task.TaskBindToProjectRequest;
import ru.t1.dkozoriz.tm.dto.request.task.TaskUnbindToProjectRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    public TaskUnbindFromProjectCommand() {
        super("unbind-task-to-project", "unbind task to project.");
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        taskEndpoint.taskUnbindToProject(new TaskUnbindToProjectRequest(getToken(), projectId, taskId));
    }

}
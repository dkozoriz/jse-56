package ru.t1.dkozoriz.tm.command.user;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserLockRequest;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class UserLockCommand extends AbstractUserCommand {

    public UserLockCommand() {
        super("user-lock", "user lock.");
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        userEndpoint.userLock(new UserLockRequest(getToken(), login));
    }
}

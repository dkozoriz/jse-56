package ru.t1.dkozoriz.tm.command.user;

import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.user.UserLoginRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dkozoriz.tm.enumerated.Role;

@Component
public final class UserLogoutCommand extends AbstractUserCommand {

    public UserLogoutCommand() {
        super("logout", "logout current user.");
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        authEndpoint.logout(new UserLogoutRequest(getToken()));
    }

}
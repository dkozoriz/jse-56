package ru.t1.dkozoriz.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.Socket;

public interface IEndpointClient {

    @Nullable
    Socket connect() throws IOException;

    void disconnect() throws IOException;

    void setSocket(@NotNull Socket socket);

}

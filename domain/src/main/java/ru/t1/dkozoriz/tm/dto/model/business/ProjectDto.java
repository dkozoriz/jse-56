package ru.t1.dkozoriz.tm.dto.model.business;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.enumerated.Status;

@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity
@Table(name = "tm_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectDto extends BusinessModelDto implements IWBS {

    private static final long serialVersionUID = 1;

    public ProjectDto(@NotNull final String name) {
        super(name);
    }

    public ProjectDto(@NotNull final String name, @NotNull final Status status) {
        super(name, status);
    }

    public ProjectDto(@NotNull final String name, @NotNull final String description) {
        super(name, description);
    }

}
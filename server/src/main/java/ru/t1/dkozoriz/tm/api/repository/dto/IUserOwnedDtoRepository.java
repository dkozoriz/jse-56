package ru.t1.dkozoriz.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.UserOwnedModelDto;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public interface IUserOwnedDtoRepository<T extends UserOwnedModelDto> extends IAbstractDtoRepository<T> {

    @NotNull
    EntityManager getEntityManager();

    void add(@NotNull String userId, T model);

    @NotNull
    List<T> findAll(@NotNull String userId);

    @Nullable
    T findById(@NotNull String userId, @NotNull String id);

    @Nullable
    T findByIndex(@NotNull String userId, @NotNull Integer index);

    long getSize(@NotNull String userId);

    void clear(@NotNull String userId);

    void update(@NotNull String userId, T model);

    void remove(@NotNull String userId, T model);

    void removeAll(@NotNull String userId, @NotNull Collection<T> models);

    void addAll(@NotNull String userId, @NotNull Collection<T> models);
}

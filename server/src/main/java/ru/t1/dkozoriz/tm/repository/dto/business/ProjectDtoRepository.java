package ru.t1.dkozoriz.tm.repository.dto.business;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.api.repository.dto.business.IProjectDtoRepository;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectDtoRepository extends BusinessDtoRepository<ProjectDto> implements IProjectDtoRepository {

    @Override
    @NotNull
    protected Class<ProjectDto> getClazz() {
        return ProjectDto.class;
    }

}
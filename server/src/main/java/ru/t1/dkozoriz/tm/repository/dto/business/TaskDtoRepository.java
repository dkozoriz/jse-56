package ru.t1.dkozoriz.tm.repository.dto.business;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.api.repository.dto.business.ITaskDtoRepository;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDto;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class TaskDtoRepository extends BusinessDtoRepository<TaskDto> implements ITaskDtoRepository {

    @Override
    @NotNull
    protected Class<TaskDto> getClazz() {
        return TaskDto.class;
    }

    @Override
    @NotNull
    public List<TaskDto> findAllByProjectId(@Nullable final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "SELECT m FROM TaskDto m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}
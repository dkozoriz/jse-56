package ru.t1.dkozoriz.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.dkozoriz.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.dkozoriz.tm.api.service.dto.ISessionDtoService;
import ru.t1.dkozoriz.tm.dto.model.SessionDto;

import javax.persistence.EntityManager;

@Service
@NoArgsConstructor
public final class SessionDtoService extends UserOwnedDtoService<SessionDto> implements ISessionDtoService {

    private final static String NAME = "Session";

    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    protected ISessionDtoRepository getRepository() {
        return context.getBean(ISessionDtoRepository.class);
    }

}
package ru.t1.dkozoriz.tm.service.model.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.dkozoriz.tm.api.repository.model.IProjectRepository;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.model.business.IProjectService;
import ru.t1.dkozoriz.tm.exception.field.DescriptionEmptyException;
import ru.t1.dkozoriz.tm.exception.field.NameEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.repository.model.business.ProjectRepository;

import javax.persistence.EntityManager;

@Service
public final class ProjectService extends BusinessService<Project> implements IProjectService {

    private final static String NAME = "Project";

    @NotNull
    @Override
    protected IProjectRepository getRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
        return project;
    }

}